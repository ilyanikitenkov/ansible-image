FROM ubuntu
RUN apt update && \
    apt install -y \
    curl \
    wget \
    vim \
    python3 \
    python3-pip \
    libffi-dev \
    libssl-dev \
    openssh-server\
    supervisor
RUN python3 -m pip install ansible || \
    python3- m pip install ansible-lint
COPY ansible.cfg /etc/ansible/ansible.cfg
EXPOSE 22
CMD ["tail", "-f", "/dev/null"]